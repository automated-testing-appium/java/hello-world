import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;


public class AppiumTest {
    private static AndroidDriver<MobileElement> driver;
//    private static WebDriverWait webDriverWait;
    public static void main(String[] args) {
        try {
            startApp();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void setUpInformation() throws MalformedURLException, InterruptedException{
        String appId = "com.viettel.vtt.vn.emoneycustomer.demo";
        String basePackage = "com.viettel.vtt.vn.emoneycustomer";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName","PIXEL 3 XL");
        capabilities.setCapability("uuid","8APY0NVNC");
        capabilities.setCapability("platformName","Android");
        capabilities.setCapability("platformVersion","10");
        capabilities.setCapability("appPackage",appId);
        capabilities.setCapability("appActivity",basePackage + ".feature.splash.SplashActivity");
        capabilities.setCapability("noReset",true);

        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AndroidDriver<MobileElement>(url,capabilities);
        System.out.println("Application started....");
//        webDriverWait = new WebDriverWait(driver,1000);
        Thread.sleep(3000);
    }

    private static void startApp() throws MalformedURLException, InterruptedException {

        setUpInformation();

        processClickById("com.viettel.vtt.vn.emoneycustomer.demo:id/lnTelecomService");
        processClickById("com.viettel.vtt.vn.emoneycustomer.demo:id/lnMetfoneExchangeData");
//        processClickByXPath("//android.widget.LinearLayout[contains(@bounds,'[42,1296][475,1553]')]");
        processClickByXPath("//android.widget.LinearLayout[@bounds='[42,1296][475,1553]']");


    }

    private static void processClickById(String id) throws InterruptedException {

        MobileElement element = driver.findElementById(id);
//        webDriverWait.until(ExpectedConditions.visibilityOf(element));
        element.click();
//        driver.findElementById(id).click();
        Thread.sleep(1000);
    }
    private static void processClickByXPath(String xPath) throws InterruptedException {
        driver.findElementByXPath(xPath).click();
        Thread.sleep(1000);
    }
}
